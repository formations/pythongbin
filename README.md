# pythonGbin

The goal of this projet is to show a way you can use to access to Gbin data in a python program.

To run the project, just open the Jupyter noteboot in python directory and execute it. The projet compile the java project associated with gradle, you don't need to install gradle.

## Gradle

Grale is used to compile the java code in a fatjar.
It is possible to use pyjnius without this code and fatjar, but it is necessary to write more code to open or write gbin files and create object from the MDB

You can easly change the dependancies in the build.gradle file :
```
dependencies {
    implementation 'gaia.cu4:nss-bl:21.4'
}
```
and add all dependancies necessary to open or write gbin with your datamodel.
