package gaia.cu4.tools;

import gaia.cu1.tools.exception.GaiaException;
import gaia.cu4.nss.epochparams.dm.StarObject;
import gaia.cu4.nss.epochparams.dm.Transit;
import java.util.Arrays;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author nl
 */
public class GbinToolsTest {
    
    public GbinToolsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void create() throws ClassNotFoundException, GaiaException {
        GbinTools<StarObject> gbinTools = new GbinTools<>("gaia.cu4.nss.epochparams.dm.StarObject");
        StarObject t = gbinTools.newInstance();
        Assert.assertEquals(0L, t.getSourceId());
    }
    
    @Test(expected = ClassNotFoundException.class)
    public void createBad() throws ClassNotFoundException, GaiaException {
        GbinTools<StarObject> gbinTools = new GbinTools<>("XXXXX");
    }
    
    
    @Test
    public void arrayOf() throws ClassNotFoundException, GaiaException {
        GbinTools<Transit> gbinTools = new GbinTools<>("gaia.cu4.nss.epochparams.dm.Transit");
        
        Transit t1 = gbinTools.newInstance();
        t1.setTransitId(123L);
        Transit t2 = gbinTools.newInstance();
        t2.setTransitId(456L);
        Transit t3 = gbinTools.newInstance();
        t3.setTransitId(789L);
        
        Transit[] t = gbinTools.arrayOf(t1, t2, t3);
        
        Arrays.stream(t).forEach(v -> System.out.println(v.getTransitId()));
    }
}
