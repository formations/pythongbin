package gaia.cu4.tools;

import gaia.cu1.tools.dal.ObjectFactory;
import gaia.cu1.tools.dal.gbin.GbinFactory;
import gaia.cu1.tools.dal.gbin.GbinReader;
import gaia.cu1.tools.dal.gbin.GbinWriter;
import gaia.cu1.tools.dm.GaiaRoot;
import gaia.cu1.tools.exception.GaiaException;
import gaia.cu1.tools.util.props.PropertyLoader;
import java.io.File;
import java.util.ArrayList;

/**
 *
 * @author nl
 * @param <T>
 * @version $Id$
 */
public class GbinTools<T extends GaiaRoot> {

    private ObjectFactory<T> factory;

    public GbinTools(String clazz) throws ClassNotFoundException, GaiaException {
        PropertyLoader.load();
        if (clazz.contains("impl")) {
            throw new IllegalStateException(String.format("Waiting interface, not implementation of class %s", clazz));
        }
        this.factory = new ObjectFactory<>(Class.forName(clazz));
    }

    public void writeGbin(String filename, ArrayList<T> objects) {
        try {
            GbinWriter writer = GbinFactory.getGbinWriter();
            writer.writeToFile(new File(filename), objects, false);
        } catch (GaiaException e) {
            throw new IllegalStateException(e);
        }
    }

    public ArrayList<T> readGbin(String filename) {
        ArrayList<T> list = new ArrayList<>();
        try (GbinReader<T> reader = GbinFactory.getGbinReader(new File(filename))) {
            reader.readAllToList(list);
            return list;

        } catch (GaiaException e) {
            throw new IllegalStateException(e);
        }
    }

    public T newInstance() throws GaiaException {
        return factory.getObject();
    }
    
    @SuppressWarnings("unchecked")
    public T[] arrayOf(T... objects) {
        return objects;
    }
}
